/*
 *    Copyright 2016 Jose Luis Cercos-Pita
 *
 *    This file is part of NASAL-GEOM.
 *
 *    NASAL-GEOM is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    NASAL-GEOM is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with NASAL-GEOM.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @brief Give the linearized id of a pixel from the x, y and z indexes
 * information
 * @param i x index
 * @param j y index
 * @param k z index
 * @param nx Number of pixels in x direction
 * @param ny Number of pixels in y direction
 * @param nz Number of pixels in z direction
 * @return The linearized index
 */
int index(int i, int j, int k, int nx, int ny, int nz)
{
    if(i >= nx)
        i -= nx;
    else if(i < 0)
        i += nx;
    if(j >= ny)
        j -= ny;
    else if(j < 0)
        j += ny;
    if(k >= nz)
        k -= nz;
    else if(k < 0)
        k += nz;
    return k + j * nz + i * nz * ny;
}


/** @brief Compute the gradients of the input image
 *
 * The gradient is computed for the whole working package, plus an halo of one
 * element to allow computing subsequent gradients later
 *
 * @param Img Input image
 * @param Ix Partial derivative along x axis
 * @param Iy Partial derivative along y axis
 * @param Iz Partial derivative along z axis
 * @param nx Number of pixels in x direction for this working package
 * @param ny Number of pixels in y direction for this working package
 * @param nz Number of pixels in z direction for this working package
 */
__kernel void grad(__global const float *Img,
                   __global float *Ix,
                   __global float *Iy,
                   __global float *Iz,
                   int nx,
                   int ny,
                   int nz)
{
    const int i = get_global_id(0);
    const int j = get_global_id(1);
    const int k = get_global_id(2);
    if((i >= nx + 2) || (j >= ny + 2) || (k >= nz + 2))
        return;

    // Get the index in the working package
    const int gid = index(i, j, k, nx + 2, ny + 2, nz + 2);

    // We should add the halo layer offset to the image reading pixel indexes
    const int I = i + 1;
    const int J = j + 1;
    const int K = k + 1;

    // Compute the derivatives
    Ix[gid] = 0.5f * (Img[index(I + 1, J, K, nx + 4, ny + 4, nz + 4)] -
                      Img[index(I - 1, J, K, nx + 4, ny + 4, nz + 4)]);
    Iy[gid] = 0.5f * (Img[index(I, J + 1, K, nx + 4, ny + 4, nz + 4)] -
                      Img[index(I, J - 1, K, nx + 4, ny + 4, nz + 4)]);
    Iz[gid] = 0.5f * (Img[index(I, J, K + 1, nx + 4, ny + 4, nz + 4)] -
                      Img[index(I, J, K - 1, nx + 4, ny + 4, nz + 4)]);
}


/** @brief Compute the conductivity factor c
 *
 * You can adjust K_C to set the sensitivity to the edges (the bigger value the
 * lower sensitive will be the algorithm)
 *
 * @param Ix Partial derivative along x axis
 * @param Iy Partial derivative along y axis
 * @param Iz Partial derivative along z axis
 * @param c Conductivity factor
 * @param beta Beta factor (Bigger than 2 / (1 + sqrt(5)))
 * @param nx Number of pixels in x direction for this working package
 * @param ny Number of pixels in y direction for this working package
 * @param nz Number of pixels in z direction for this working package
 */
__kernel void c(__global const float *Ix,
                __global const float *Iy,
                __global const float *Iz,
                __global float *c,
                int nx,
                int ny,
                int nz,
                float beta)
{
    const int i = get_global_id(0);
    const int j = get_global_id(1);
    const int k = get_global_id(2);
    if((i >= nx + 2) || (j >= ny + 2) || (k >= nz + 2))
        return;

    // Get the index in the working package
    const int gid = index(i, j, k, nx + 2, ny + 2, nz + 2);

    const float grad_mod = sqrt(Ix[gid] * Ix[gid] +
                                Iy[gid] * Iy[gid] +
                                Iz[gid] * Iz[gid]) / beta;

    // Uncomment the most convenient one (for very low values of K_C, just the
    // first one may properly work)
    c[gid] = (2.f + grad_mod) / (1.f + grad_mod * grad_mod);
}


/** @brief Compute the first part of dIdt
 *
 * The time derivative is composed by two terms:
 * \f$ \nabla c \cdot \nabla I \f$ and
 * \f$ c \Delta I \f$
 *
 * This function takes care of the first one
 *
 * @param Ix Partial derivative along x axis
 * @param Iy Partial derivative along y axis
 * @param Iz Partial derivative along z axis
 * @param c Conductivity factor
 * @param dIdt Conductivity factor
 * @param nx Number of pixels in x direction for this working package
 * @param ny Number of pixels in y direction for this working package
 * @param nz Number of pixels in z direction for this working package
 */
__kernel void dIdt_stage1(__global const float *Ix,
                          __global const float *Iy,
                          __global const float *Iz,
                          __global const float *c,
                          __global float *dIdt,
                          int nx,
                          int ny,
                          int nz)
{
    const int i = get_global_id(0);
    const int j = get_global_id(1);
    const int k = get_global_id(2);
    if((i >= nx) || (j >= ny) || (k >= nz))
        return;

    // Get the index in the working package
    const int gid = index(i, j, k, nx, ny, nz);

    // We should add the halo layer offset to the image reading pixel indexes
    const int I = i + 1;
    const int J = j + 1;
    const int K = k + 1;

    // Compute the derivatives
    const float cx = 0.5f * (c[index(I + 1, J, K, nx + 2, ny + 2, nz + 2)] -
                             c[index(I - 1, J, K, nx + 2, ny + 2, nz + 2)]);
    const float cy = 0.5f * (c[index(I, J + 1, K, nx + 2, ny + 2, nz + 2)] -
                             c[index(I, J - 1, K, nx + 2, ny + 2, nz + 2)]);
    const float cz = 0.5f * (c[index(I, J, K + 1, nx + 2, ny + 2, nz + 2)] -
                             c[index(I, J, K - 1, nx + 2, ny + 2, nz + 2)]);

    // Set the time derivative term
    dIdt[gid] = cx * Ix[index(I, J, K, nx + 2, ny + 2, nz + 2)] +
                cy * Iy[index(I, J, K, nx + 2, ny + 2, nz + 2)] +
                cz * Iz[index(I, J, K, nx + 2, ny + 2, nz + 2)];
}


/** @brief Compute the first part of dIdt
 *
 * The time derivative is composed by two terms:
 * \f$ \nabla c \cdot \nabla I \f$ and
 * \f$ c \Delta I \f$
 *
 * This function takes care of the latter
 *
 * @param Ix Partial derivative along x axis
 * @param Iy Partial derivative along y axis
 * @param Iz Partial derivative along z axis
 * @param c Conductivity factor
 * @param dIdt Conductivity factor
 * @param nx Number of pixels in x direction for this working package
 * @param ny Number of pixels in y direction for this working package
 * @param nz Number of pixels in z direction for this working package
 */
__kernel void dIdt_stage2(__global const float *Ix,
                          __global const float *Iy,
                          __global const float *Iz,
                          __global const float *c,
                          __global float *dIdt,
                          int nx,
                          int ny,
                          int nz)
{
    const int i = get_global_id(0);
    const int j = get_global_id(1);
    const int k = get_global_id(2);
    if((i >= nx) || (j >= ny) || (k >= nz))
        return;

    // Get the index in the working package
    const int gid = index(i, j, k, nx, ny, nz);

    // We should add the halo layer offset to the image reading pixel indexes
    const int I = i + 1;
    const int J = j + 1;
    const int K = k + 1;

    // Compute the derivatives
    const float Ixx = 0.5f * (Ix[index(I + 1, J, K, nx + 2, ny + 2, nz + 2)] -
                              Ix[index(I - 1, J, K, nx + 2, ny + 2, nz + 2)]);
    const float Iyy = 0.5f * (Iy[index(I, J + 1, K, nx + 2, ny + 2, nz + 2)] -
                              Iy[index(I, J - 1, K, nx + 2, ny + 2, nz + 2)]);
    const float Izz = 0.5f * (Iz[index(I, J, K + 1, nx + 2, ny + 2, nz + 2)] -
                              Iz[index(I, J, K - 1, nx + 2, ny + 2, nz + 2)]);

    // Set the time derivative term
    dIdt[gid] += c[index(I, J, K, nx + 2, ny + 2, nz + 2)] * (Ixx + Iyy + Izz);
}


/** @brief Integrate in time the diffusion equation solution
 *
 * @param c Conductivity factor
 * @param Img Input image
 * @param nx Number of pixels in x direction for this working package
 * @param ny Number of pixels in y direction for this working package
 * @param nz Number of pixels in z direction for this working package
 * @param ht Time step
 */
__kernel void time_step(__global const float *dIdt,
                        __global float *Img,
                        int nx,
                        int ny,
                        int nz,
                        float ht)
{
    const int i = get_global_id(0);
    const int j = get_global_id(1);
    const int k = get_global_id(2);
    if((i >= nx) || (j >= ny) || (k >= nz))
        return;

    // Get the index in the working package
    const int gid_in = index(i, j, k, nx, ny, nz);

    // We should add the halo layer offset to the image writing pixel indexes
    const int I = i + 2;
    const int J = j + 2;
    const int K = k + 2;
    const int gid_out = index(I, J, K, nx + 4, ny + 4, nz + 4);

    Img[gid_out] += ht * dIdt[gid_in];
}
