/*
 *    Copyright 2016 Jose Luis Cercos-Pita
 *
 *    This file is part of NASAL-GEOM.
 *
 *    NASAL-GEOM is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    NASAL-GEOM is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with NASAL-GEOM.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @brief Give the linearized id of a pixel from the x, y and z indexes
 * information
 * @param i x index
 * @param j y index
 * @param k z index
 * @param nx Number of pixels in x direction
 * @param ny Number of pixels in y direction
 * @param nz Number of pixels in z direction
 * @return The linearized index
 */
int index(int i, int j, int k, int nx, int ny, int nz)
{
    return k + j * nz + i * nz * ny;
}


/** @brief Interpolate the values
 *
 * This function is computing the Shepard renormalization factor as well
 *
 * @param Img Input image
 * @param I_out Output interpolated values (non-renormalized)
 * @param shepard Shepard renormalization factor
 * @param nx Number of pixels in x direction for this working package
 * @param ny Number of pixels in y direction for this working package
 * @param nz Number of pixels in z direction for this working package
 * @param s_d Standard deviation for range distance factor. A larger value
 * results in averaging of pixels with larger spatial differences
 * @param s_r Standard deviation for grayvalue/color difference factor
 * (radiometric similarity factor). The larger is this value the less edge
 * preserving the algorithm will result
 * @param win_radius Window interpolation radius
 */
__kernel void interpolate(__global const float *Img,
                          __global float *I_out,
                          __global float *shepard,
                          int nx,
                          int ny,
                          int nz,
                          float s_d,
                          float s_r,
                          int win_radius)
{
    const int i = get_global_id(0);
    const int j = get_global_id(1);
    const int k = get_global_id(2);
    if((i >= nx) || (j >= ny) || (k >= nz))
        return;

    // Get the index of the output pixel
    const int gid_out = index(i, j, k, nx, ny, nz);

    // Get the input equivalent pixel index, and image sample
    const float I_i = Img[index(i + win_radius, j + win_radius, k + win_radius,
                                nx + 2 * win_radius,
                                ny + 2 * win_radius,
                                nz + 2 * win_radius)];

    // Interpolate
    I_out[gid_out] = 0.f;
    shepard[gid_out] = 0.f;
    int I, J, K;
    for(I = 0; I <= 2 * win_radius; I++){
        const float dx = I - win_radius;
        const float dx2 = dx * dx;
        for(J = 0; J <= 2 * win_radius; J++){
            const float dy = J - win_radius;
            const float dy2 = dy * dy;
            for(K = 0; K <= 2 * win_radius; K++){
                const float dz = K - win_radius;
                const float dz2 = dz * dz;

                // Get the distance
                const float d2 = dx2 + dy2 + dz2;

                // Get the radiosity difference
                const float I_j = Img[index(I + i, J + j, K + k,
                                            nx + 2 * win_radius,
                                            ny + 2 * win_radius,
                                            nz + 2 * win_radius)];
                const float dI = I_j - I_i;

                // Compute the kernel value
                const float w = exp(- d2 * s_d - dI * dI * s_r);

                // Store the results
                I_out[gid_out] += I_j * w;
                shepard[gid_out] += w;
            }
        }
    }
}


/** @brief Renormalize the resulting interpolated image
 *
 * @param I Interpolated image
 * @param shepard Shepard renormalization factor
 * @param nx Number of pixels in x direction for this working package
 * @param ny Number of pixels in y direction for this working package
 * @param nz Number of pixels in z direction for this working package
 */
__kernel void renormalize(__global float *I,
                          __global const float *shepard,
                          int nx,
                          int ny,
                          int nz)
{
    const int i = get_global_id(0);
    const int j = get_global_id(1);
    const int k = get_global_id(2);
    if((i >= nx) || (j >= ny) || (k >= nz))
        return;

    const int gid = index(i, j, k, nx, ny, nz);
    I[gid] /= shepard[gid];
}
